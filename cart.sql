create database cart;

use cart;

drop table  if exists product;
create table product(
	id int(11) default null,
    name varchar(50) default null,
    price float default null
)engine=innodb default charset=utf8;

insert into product values(1,"笔记本电脑", 5000);
insert into product values(2,"水杯", 50);
insert into product values(3,"自行车", 600);
insert into product values(4,"鞋子", 400);

create table user(
id int,
name varchar(50),
password varchar(50)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into user values(1,'tom','123');

create table order_(
	id int auto_increment,
    uid int,
    primary key(id)
    );

create table orderitem(
	id int auto_increment,
    pid int,
    num int,
    oid int,
	primary key(id)
    );